﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AVFoundation;
using CoreAnimation;
using CoreFoundation;
using CoreGraphics;
using CoreImage;
using Foundation;
using UIKit;
using WaterMark.CustomCamera;
using Xamarin.Forms;
using static WaterMark.Helper.DataModel;

namespace WaterMark.iOS.Custom_Camera
{
	public class UICameraPreview : UIView, IAVCaptureMetadataOutputObjectsDelegate
	{
		private CALayer _overlayLayer;
		private Dictionary<nint, CALayer> _faceLayers = new Dictionary<nint, CALayer>();
		protected AVCaptureMetadataOutput FaceDetectionOutput { get; set; }

		AVCaptureVideoPreviewLayer previewLayer;
		public AVCaptureDevice[] videoDevices;
		CameraOptions cameraOptions;
		public AVCaptureStillImageOutput stillImageOutput;
		public AVCaptureDeviceInput captureDeviceInput;
		public AVCaptureDevice device;

		public event EventHandler<EventArgs> Tapped;

		public AVCaptureSession CaptureSession { get; set; }

		public bool IsPreviewing { get; set; }

		public AVCaptureStillImageOutput CaptureOutput { get; set; }



		public UICameraPreview(CameraOptions options)
		{
			cameraOptions = options;
			IsPreviewing = false;
			Initialize();
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			if (previewLayer != null)
				previewLayer.Frame = Bounds;
		}

		public override void TouchesBegan(NSSet touches, UIEvent evt)
		{
			base.TouchesBegan(touches, evt);
			OnTapped();
		}

		protected virtual void OnTapped()
		{
			var eventHandler = Tapped;
			if (eventHandler != null)
			{
				eventHandler(this, new EventArgs());
			}
		}


		void Initialize()
		{
			CaptureSession = new AVCaptureSession();
			previewLayer = new AVCaptureVideoPreviewLayer(CaptureSession)
			{
				Frame = Bounds,
				VideoGravity = AVLayerVideoGravity.ResizeAspectFill
			};

			videoDevices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video);
			var cameraPosition = (cameraOptions == CameraOptions.Front) ? AVCaptureDevicePosition.Front : AVCaptureDevicePosition.Back;
			device = videoDevices.FirstOrDefault(d => d.Position == cameraPosition);

			if (device == null)
			{
				return;
			}

			NSError error;
			captureDeviceInput = new AVCaptureDeviceInput(device, out error);
			CaptureSession.AddInput(captureDeviceInput);

			var dictionary = new NSMutableDictionary();
			dictionary[AVVideo.CodecKey] = new NSNumber((int)AVVideoCodec.JPEG);
			stillImageOutput = new AVCaptureStillImageOutput()
			{
				OutputSettings = new NSDictionary()
			};
			CaptureSession.AddOutput(stillImageOutput);
			Layer.AddSublayer(previewLayer);
			CaptureSession.StartRunning();
			IsPreviewing = true;
		}

		public async Task CapturePhoto()
		{
			try
			{
				var videoConnection = stillImageOutput.ConnectionFromMediaType(AVMediaType.Video);
				var sampleBuffer = await stillImageOutput.CaptureStillImageTaskAsync(videoConnection);
				var jpegData = AVCaptureStillImageOutput.JpegStillToNSData(sampleBuffer);
				var photo = new UIImage(jpegData);
				var rotatedPhoto = RotateImage(photo, 180f);
				var img = rotatedPhoto;
				FaceDetectionCheking(photo);
				//CALayer layer = new CALayer
				//{

				//	ContentsScale = 1.0f,
				//	Frame = Bounds,
				//	Contents = rotatedPhoto.CGImage
				//};

				//CaptureSession.StopRunning();


				//photo.SaveToPhotosAlbum((image, error) =>
				//{
				//	if (!string.IsNullOrEmpty(error?.LocalizedDescription))
				//	{
				//		Console.Error.WriteLine($"\t\t\tError: {error.LocalizedDescription}");
				//	}
				//});

			}
			catch (Exception ex)
			{
			}
			//MainPage.UpdateSource(UIImageFromLayer(layer).AsJPEG().AsStream());
			//MainPage.UpdateImage(UIImageFromLayer(layer).AsJPEG().AsStream());
		}

		private void UpdatePreviewLayer(AVCaptureConnection layer,
		   AVCaptureVideoOrientation orientation)
		{
			layer.VideoOrientation = orientation;
			previewLayer.Frame = this.Bounds;
		}


		public UIImage RotateImage(UIImage image, float degree)
		{
			float Radians = degree * (float)Math.PI / 180;

			UIView view = new UIView(frame: new CGRect(0, 0, image.Size.Width, image.Size.Height));
			CGAffineTransform t = CGAffineTransform.MakeRotation(Radians);
			view.Transform = t;
			CGSize size = view.Frame.Size;

			UIGraphics.BeginImageContext(size);
			CGContext context = UIGraphics.GetCurrentContext();

			context.TranslateCTM(size.Width / 2, size.Height / 2);
			context.RotateCTM(Radians);
			context.ScaleCTM(1, -1);

			context.DrawImage(new CGRect(-image.Size.Width / 2, -image.Size.Height / 2, image.Size.Width, image.Size.Height), image.CGImage);

			UIImage imageCopy = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			return imageCopy;
		}

		UIImage ImageFromLayer(CALayer layer)
		{
			UIGraphics.BeginImageContextWithOptions(
				layer.Frame.Size,
				layer.Opaque,
				0);
			layer.RenderInContext(UIGraphics.GetCurrentContext());
			var outputImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return outputImage;
		}


		protected void SetupFaceDetection()
		{
			FaceDetectionOutput = new AVCaptureMetadataOutput();
			if (CaptureSession.CanAddOutput(FaceDetectionOutput))
			{
				CaptureSession.AddOutput(FaceDetectionOutput);

				if (FaceDetectionOutput.AvailableMetadataObjectTypes.HasFlag(
					AVMetadataObjectType.Face))
				{
					FaceDetectionOutput.MetadataObjectTypes = AVMetadataObjectType.Face;
					FaceDetectionOutput.SetDelegate(this, DispatchQueue.MainQueue);
				}
				else
				{
					CaptureSession.RemoveOutput(FaceDetectionOutput);
					FaceDetectionOutput.Dispose();
					FaceDetectionOutput = null;
				}
			}
		}

		/// <summary>
		/// Implementation of <see cref="IAVCaptureMetadataOutputObjectsDelegate"/>.
		/// 
		/// Used by <see cref="AVCaptureMetadataOutput"/> as callback when <see cref="AVMetadataObject"/>s
		/// are found in Session.
		/// </summary>
		/// <param name="captureOutput">Capture output.</param>
		/// <param name="metadataObjects">Metadata objects.</param>
		/// <param name="connection">Connection.</param>
		[Export("captureOutput:didOutputMetadataObjects:fromConnection:")]
		public void DidOutputMetadataObjects(AVCaptureMetadataOutput captureOutput,
		AVMetadataObject[] metadataObjects, AVCaptureConnection connection)
		{
			RemoveFaces();

			// only interested in faces
			foreach (var metadata in metadataObjects.OfType<AVMetadataFaceObject>())
			{
				// transform the metadata object to adjust bounds
				var transformed = previewLayer.GetTransformedMetadataObject(metadata);

				// used later, from this we will get yaw and roll angles
				var face = (AVMetadataFaceObject)transformed;

				// create a new layer
				var faceLayer = CreateFaceLayer();
				// set the transformed bounds
				faceLayer.Frame = transformed.Bounds;

				faceLayer.Transform = CATransform3D.Identity;
				if (face.HasRollAngle)
				{
					var transform = RollTransform(face.RollAngle);
					faceLayer.Transform = faceLayer.Transform.Concat(transform);
				}

				if (face.HasYawAngle)
				{
					var transform = YawTransform(face.YawAngle);
					faceLayer.Transform = faceLayer.Transform.Concat(transform);
				}

				// display it!
				previewLayer.AddSublayer(faceLayer);
			}
		}

		private CALayer CreateFaceLayer()
		{
			var faceLayer = new CALayer
			{
				BorderColor = UIColor.White.CGColor,
				BorderWidth = 2,
				CornerRadius = 5
			};

			return faceLayer;
		}

		void RemoveFaces()
		{
			foreach (var layer in previewLayer.Sublayers)
				layer.RemoveFromSuperLayer();
		}

		private void RemoveLostFaces(IEnumerable<nint> lostFaces)
		{
			foreach (var faceId in lostFaces)
			{
				if (_faceLayers.ContainsKey(faceId))
				{
					var layer = _faceLayers[faceId];
					_faceLayers.Remove(faceId);

					layer.RemoveFromSuperLayer();
					layer.Dispose();
					layer = null;
				}
			}
		}

		private CATransform3D RollTransform(nfloat rollAngle)
		{
			var radians = rollAngle * (nfloat)Math.PI / 180f;
			return CATransform3D.MakeRotation(radians, 0.0f, 0.0f, 1.0f);
		}

		private CATransform3D YawTransform(nfloat yawAngle)
		{
			var radians = yawAngle * (nfloat)Math.PI / 180f;

			var yawTransform = CATransform3D.MakeRotation(radians, 0.0f, -1.0f, 0.0f);
			var orientationTransform = OrientationTransform();
			return orientationTransform.Concat(yawTransform);
		}


		private async Task FaceDetectionCheking(UIImage photo)
		{
			await Task.Run(() =>
			{
				try
				{
					var imageFile = photo;
					var context = new CIContext();
					var detector = CIDetector.CreateFaceDetector(context, true);
					var ciImage = CIImage.FromCGImage(imageFile.CGImage);
					var features = detector.FeaturesInImage(ciImage);

					if (features.Length == 0)
					{
						MessagingCenter.Send(new MyMessage() { Myvalue = "No Face Detected !" }, "PictureTakenFailed");
					}
					else if (features.Length == 1)
					{
						Stream ImageStream = imageFile.AsJPEG(0.3f).AsStream();

						var documents_path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
						documents_path = Path.Combine(documents_path, "temp");
						Directory.CreateDirectory(documents_path);

						string filePath = Path.Combine(documents_path, "AnandPhoto");
						byte[] bytes = new byte[ImageStream.Length];
						using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate))
						{
							using (ImageStream)
							{
								ImageStream.Read(bytes, 0, (int)ImageStream.Length);
							}
							int length = bytes.Length;
							fs.Write(bytes, 0, length);
						}


						MessagingCenter.Send(new MyMessage() { Myvalue = filePath }, "PictureTaken");
						CaptureSession.StopRunning();

					}
					else if (features.Length > 1)
					{
						MessagingCenter.Send(new MyMessage() { Myvalue = "Multiple Faces Detected !" }, "PictureTakenFailed");
					}
					//Console.WriteLine("Found " + features.Length + " faces");
				}

				catch (Exception ex)
				{

				}

			});


		}

		private CATransform3D OrientationTransform()
		{
			nfloat angle = 0.0f;
			switch (UIDevice.CurrentDevice.Orientation)
			{
				case UIDeviceOrientation.PortraitUpsideDown:
					angle = (nfloat)Math.PI;
					break;
				case UIDeviceOrientation.LandscapeRight:
					angle = (nfloat)(-Math.PI / 2.0f);
					break;
				case UIDeviceOrientation.LandscapeLeft:
					angle = (nfloat)Math.PI / 2.0f;
					break;
				default:
					angle = 0.0f;
					break;
			}

			return CATransform3D.MakeRotation(angle, 0.0f, 0.0f, 1.0f);
		}

	}
}