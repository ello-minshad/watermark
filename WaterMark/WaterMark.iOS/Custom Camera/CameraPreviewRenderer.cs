﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AVFoundation;
using Foundation;
using UIKit;
using WaterMark.CustomCamera;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CameraPreview), typeof(WaterMark.iOS.Custom_Camera.CameraPreviewRenderer))]
namespace WaterMark.iOS.Custom_Camera
{
	public class CameraPreviewRenderer : ViewRenderer<CameraPreview, UICameraPreview>, IAVCaptureMetadataOutputObjectsDelegate
	{
		UICameraPreview uiCameraPreview;

		AVCaptureSession captureSession;
		AVCaptureDeviceInput captureDeviceInput;
		AVCaptureStillImageOutput stillImageOutput;

		protected override void OnElementChanged(ElementChangedEventArgs<CameraPreview> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null)
			{
				// Unsubscribe
				uiCameraPreview.Tapped -= OnCameraPreviewTapped;
			}
			if (e.NewElement != null)
			{
				if (Control == null)
				{
					uiCameraPreview = new UICameraPreview(e.NewElement.Camera);
					SetNativeControl(uiCameraPreview);
					MessagingCenter.Subscribe<Object>(this, "CaptureClick", async (sender) =>
					{
						try
						{
							var data = new AVCaptureMetadataOutputObjectsDelegate();
							await uiCameraPreview.CapturePhoto();
							//await Task.Run(() => takephoto());
						}
						catch (Exception ex)
						{
							return;
						}

					});
				}
				MessagingCenter.Subscribe<Object>(this, "RetryClick", (sender) =>
				{
					Device.BeginInvokeOnMainThread(() =>
					{
						uiCameraPreview.CaptureSession.StartRunning();
						uiCameraPreview.IsPreviewing = true;
					});
				});
				MessagingCenter.Subscribe<Object>(this, "FlipClick", (sender) =>
				{
					try
					{
						var devicePosition = uiCameraPreview.captureDeviceInput.Device.Position;
						if (devicePosition == AVCaptureDevicePosition.Front)
						{
							devicePosition = AVCaptureDevicePosition.Back;
						}
						else
						{
							devicePosition = AVCaptureDevicePosition.Front;
						}
						uiCameraPreview.device = uiCameraPreview.videoDevices.FirstOrDefault(d => d.Position == devicePosition);
						uiCameraPreview.CaptureSession.BeginConfiguration();
						uiCameraPreview.CaptureSession.RemoveInput(uiCameraPreview.captureDeviceInput);
						uiCameraPreview.captureDeviceInput = AVCaptureDeviceInput.FromDevice(uiCameraPreview.device);
						uiCameraPreview.CaptureSession.AddInput(uiCameraPreview.captureDeviceInput);
						uiCameraPreview.CaptureSession.CommitConfiguration();

					}
					catch (Exception ex)
					{
						var abc = ex.InnerException.Message;
					}

				});
				uiCameraPreview.Tapped += OnCameraPreviewTapped;
			}
		}

		[Export("captureOutput:didOutputMetadataObjects:fromConnection:")]
		public void DidOutputMetadataObjects(AVCaptureMetadataOutput captureOutput, AVMetadataObject[] metadataObjects, AVCaptureConnection connection)
		{
			// faces are dumped here...
		}




		public async void takephoto()
		{
			try
			{
				var videoConnection = stillImageOutput.ConnectionFromMediaType(AVMediaType.Video);
				var sampleBuffer = await stillImageOutput.CaptureStillImageTaskAsync(videoConnection);
				var jpegImage = AVCaptureStillImageOutput.JpegStillToNSData(sampleBuffer);
				var photo = new UIImage(jpegImage);

				//photo.SaveToPhotosAlbum((image, error) =>
				//{
				//	if (!string.IsNullOrEmpty(error?.LocalizedDescription))
				//	{
				//		Console.Error.WriteLine($"\t\t\tError: {error.LocalizedDescription}");
				//	}
				//});

			}
			catch (Exception ex)
			{
				Console.Error.WriteLine(" Exception" + ":" + ex.Message);
			}

		}



		void OnCameraPreviewTapped(object sender, EventArgs e)
		{
			if (uiCameraPreview.IsPreviewing)
			{
				uiCameraPreview.CaptureSession.StopRunning();
				uiCameraPreview.IsPreviewing = false;
			}
			else
			{
				uiCameraPreview.CaptureSession.StartRunning();
				uiCameraPreview.IsPreviewing = true;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				Control.CaptureSession.Dispose();
				Control.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}