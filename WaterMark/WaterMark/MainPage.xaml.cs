﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using Xamarin.Forms;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using static WaterMark.Helper.DataModel;
using Xamarin.Essentials;

namespace WaterMark
{
    public partial class MainPage : ContentPage
    {
        public string ImagePath;
        private FileStream bitmapImageStream;
        SKBitmap bitmap;

        public MainPage()
        {
            InitializeComponent();
            MessegingCenterDetection();
        }

        protected async override void OnAppearing()
        {
            var status = await Permissions.RequestAsync<Permissions.Camera>();
            base.OnAppearing();
        }

        [Obsolete]
        private void addWaterMark(string Path)
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                var resizeFactor = 0.5f;
                var bitmap = SKBitmap.Decode(Path);
                var rotated = new SKBitmap(bitmap.Height, bitmap.Width);
                using (var surface = new SKCanvas(rotated))
                {
                    surface.Translate(rotated.Width, 0);
                    surface.RotateDegrees(90);
                    surface.DrawBitmap(bitmap, 0, 0);
                }

                var canvas = new SKCanvas(rotated);
                var font = SKTypeface.FromFamilyName("Arial");
                var brush = new SKPaint
                {
                    Typeface = font,
                    TextSize = 40,
                    IsAntialias = true,
                    Color = new SKColor(3, 15, 252, 255)
                };
                string normalText = " Ellobees Technologies Pvt.Ltd \n Opposite Police Station \n Chalakudy P.O \n Thrissur \n 20-2-2020 - 09:30AM";
                var spaceWidth = 60;
                float lineHeight = brush.TextSize * 1.2f;
                var lines = normalText.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                var y = bitmap.Height * resizeFactor / 6.0f;

                for (int i = 0; i < lines.Length; i++)
                {
                    var line = lines[i];
                    canvas.DrawText(line, 0, y, brush);
                    y += lineHeight;
                }

                var imageSK = SKImage.FromBitmap(rotated);
                SKData encodedData = imageSK.Encode(SKEncodedImageFormat.Jpeg, 20);
                bitmapImageStream = File.Open(Path,
                                              FileMode.Create,
                                              FileAccess.ReadWrite,
                                              FileShare.None);

                encodedData.SaveTo(bitmapImageStream);
                bitmapImageStream.Flush(true);
                bitmapImageStream.Dispose();
                source.Source = (SKImageImageSource)imageSK;
                imageSK.Dispose();
                bitmap.Dispose();
            }
            else
            {
                var resizeFactor = 0.5f;
                var bitmap = SKBitmap.Decode(Path);
                var canvas = new SKCanvas(bitmap);
                var font = SKTypeface.FromFamilyName("Montserrat");
                var brush = new SKPaint
                {
                     Style = SKPaintStyle.StrokeAndFill,
                    Typeface = font,
                    TextSize = 40,
                    IsStroke = true,
                    IsAntialias = true,
                    TextScaleX = 1.1f,
                    StrokeWidth = 3,
                    Color = Xamarin.Forms.Color.Black.ToSKColor()
                };
                string normalText = " Ellobees Technologies Pvt.Ltd \n Opposite Police Station \n Chalakudy P.O \n Thrissur \n 20-2-2020 - 09:30AM";
                var spaceWidth = 60;
                float lineHeight = brush.TextSize * 1.5f;
                var lines = normalText.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                var y = bitmap.Height * resizeFactor / .67f;

                for (int i = 0; i < lines.Length; i++)
                {
                    var line = lines[i];
                    canvas.DrawText(line, 0, y, brush);
                    y += lineHeight;
                }
                brush.Color = Xamarin.Forms.Color.FromHex("f4f4f4").ToSKColor();
                brush.StrokeWidth = 3.5f;
                y = bitmap.Height * resizeFactor / .665f;
                for (int i = 0; i < lines.Length; i++)
                {
                    var line = lines[i];
                    canvas.DrawText(line, 0, y, brush);
                    y += lineHeight;
                }
                var imageSK = SKImage.FromBitmap(bitmap);
                SKData encodedData = imageSK.Encode(SKEncodedImageFormat.Jpeg, 20);
                bitmapImageStream = File.Open(Path,
                                              FileMode.Create,
                                              FileAccess.ReadWrite,
                                              FileShare.None);

                encodedData.SaveTo(bitmapImageStream);
                bitmapImageStream.Flush(true);
                bitmapImageStream.Dispose();
                source.Source = (SKImageImageSource)imageSK;
                imageSK.Dispose();
                bitmap.Dispose();
            }
        }

        private void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            SKImageInfo info = e.Info;
            SKSurface surface = e.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear(SKColors.Aqua);

            for (float y = 0; y < info.Height; y += bitmap.Height)
                for (float x = 0; x < info.Width; x += bitmap.Width)
                {
                    canvas.DrawBitmap(bitmap, x, y);
                }
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                MessagingCenter.Send<Object>(new Object(), "CaptureClick");
            }
            catch (Exception)
            {
            }
        }

        #region Messeging Center Common Method
        private void MessegingCenterDetection()
        {
            //MessagingCenter.Subscribe<CameraPopup, string>(this, "PictureTaken", async (sender1, arg) =>
            MessagingCenter.Subscribe<MyMessage>(this, "PictureTaken", (value) =>
            {
                try
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {

                        var data = value.Myvalue;
                        ImagePath = value.Myvalue.ToString();
                        //source.Source = ImagePath;

                        addWaterMark(ImagePath);
                        await Task.Delay(2000);

                    });
                }
                catch (Exception ex)
                {
                    return;
                }
            });
            try
            {
                MessagingCenter.Subscribe<MyMessage>(this, "PictureTakenFailed", (value) =>
                {
                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await DisplayAlert("", "Image taken failed", "ok");

                    });
                });
            }
            catch (Exception)
            {
            }
            // Bug reporting
            try
            {
                MessagingCenter.Subscribe<Object>(this, "BugReported", (sender) =>
                {
                    DisplayAlert("", "Image taken failed", "ok");
                });
            }
            catch (Exception)
            {
                DisplayAlert("", "Image taken failed", "ok");
            }
        }
        #endregion Messeging Center Common Method   
    }
}
