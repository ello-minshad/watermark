﻿using Android.App;
using Android.Content;
using Android.Hardware;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaterMark.CustomCamera;
using WaterMark.Droid.Custom_Camera;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using static WaterMark.Helper.DataModel;

[assembly: ExportRenderer(typeof(WaterMark.CustomCamera.CameraPreview), typeof(CameraPreviewRenderer))]
namespace WaterMark.Droid.Custom_Camera
{
    public class CameraPreviewRenderer : ViewRenderer<CustomCamera.CameraPreview, WaterMark.Droid.Custom_Camera.CameraPreview>, Camera.IFaceDetectionListener, Camera.IPictureCallback, Camera.IShutterCallback
    {
        CameraPreview cameraPreview;
        String Picture_Name = "";
        private CameraFacing camerainfo = CameraFacing.Front;
        int DetectedFaceCount = 0;
        private bool _sendcrashreport = true;

        [get: Android.Runtime.Register("getMaxNumDetectedFaces", "()I", "GetGetMaxNumDetectedFacesHandler", ApiSince = 14)]
        public virtual int MaxNumDetectedFaces { get; }

        public CameraPreviewRenderer(Context context) : base(context)
        {
               MessagingCenter.Subscribe<Object>(this, "CaptureClick", async (sender) => {
                try
                {
                    Picture_Name = "facecapture_image" + ".jpg";
                    var CameraParaMeters = cameraPreview.camera.GetParameters();
                    if (CameraParaMeters.MaxNumDetectedFaces > 0)
                    {                     
                      await Task.Run(() => takepicture());
                                           
                    }
                    else
                    {
                        await Task.Run(() => takepicture());
                    }
                }
                catch (Exception ex)
                {
                }
            });
        }


        #region OnElement Changed
        [Obsolete]
        protected override void OnElementChanged(ElementChangedEventArgs<CustomCamera.CameraPreview> e)
        {
            try
            {
                base.OnElementChanged(e);
                if (Control == null)
                {
                    try
                    {
                        cameraPreview = new CameraPreview(Context);
                        SetNativeControl(cameraPreview);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                if (e.OldElement != null)
                {
                }
                if (e.NewElement != null)
                {
                    try
                    {
                        if (Control == null)
                        {
                            cameraPreview = new CameraPreview(Context);
                            SetNativeControl(cameraPreview);
                        }
                        Control.Preview = Camera.Open((int)e.NewElement.Camera);
                        Control.CameraID = 1;

                        var CameraParaMeters = cameraPreview.camera.GetParameters();
                        if (CameraParaMeters != null)
                        {
                            if (CameraParaMeters.MaxNumDetectedFaces > 0)
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    try
                                    {
                                        Control.Preview.SetFaceDetectionListener(this);
                                        Control.Preview.StartFaceDetection();
                                    }
                                    catch (Java.Lang.RuntimeException ex)
                                    {

                                    }
                                });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    MessagingCenter.Subscribe<object>(this, "FlipClick", (sender) =>
                    {
                        try
                        {

                            if (camerainfo == Camera.CameraInfo.CameraFacingFront)
                            {
                                camerainfo = Camera.CameraInfo.CameraFacingBack;
                                e.NewElement.Camera = CameraOptions.Rear;
                                Control.CameraID = 0;
                            }
                            else
                            {
                                camerainfo = Camera.CameraInfo.CameraFacingFront;
                                e.NewElement.Camera = CameraOptions.Front;
                                Control.CameraID = 1;
                            }
                            cameraPreview.camera.Lock();
                            cameraPreview.camera.StopPreview();
                            cameraPreview.camera.Release();
                            cameraPreview.camera = null;
                            Control.Preview = Camera.Open((int)e.NewElement.Camera);
                            SetNativeControl(cameraPreview);

                            var CameraParaMeters = cameraPreview.camera.GetParameters();
                            if (CameraParaMeters.MaxNumDetectedFaces > 0)
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    try
                                    {
                                        Control.Preview.SetFaceDetectionListener(this);
                                        Control.Preview.StartFaceDetection();
                                    }
                                    catch (Java.Lang.RuntimeException ex)
                                    {
                                        //MessagingCenter.Send<Object>(new Object(), "BugReported");

                                    }
                                });
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    });
                }
            }
            catch (Exception ex)
            {


            }
            //  MessagingCenter.Unsubscribe<Object>(this, "CaptureClick");
        }
        #endregion

        #region Disposing Camera View
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    Control.Preview.SetFaceDetectionListener(null);
                    Control.Preview.Release();
                    MessagingCenter.Unsubscribe<Object>(this, "CaptureClick");
                    MessagingCenter.Unsubscribe<Object>(this, "FlipClick");
                }
                //Device.BeginInvokeOnMainThread(base.Dispose);

            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region Face detection count
        [Obsolete]
        public void OnFaceDetection(Camera.Face[] faces, Camera camera)
        {
            try
            {
                DetectedFaceCount = faces.Length;
            }
            catch (Exception ex)
            {

            }
        }
        #endregion Face Detection count

        #region Take Picture method
        private void takepicture()
        {
            try
            {
                Control.Preview.TakePicture(this, this, this);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region Image Processing and send to shared class
        /// <summary>
        /// Above android 11 File accessing API have changes
        /// https://developer.android.com/training/data-storage#scoped-storage
        /// https://stackoverflow.com/a/58489112/9733566
        /// </summary>
        /// <param name="data"></param>
        /// <param name="camera"></param>
        public void OnPictureTaken(byte[] data, Camera camera)
        {
            try
            {
                var path = Context.GetExternalFilesDir(Android.OS.Environment.DirectoryPictures);
                File pictureFile = new File(path, "/" + Picture_Name);
                if (pictureFile.Exists())
                {
                    pictureFile.Delete();
                }
                File NewPicture = new File(path, "/" + "face.JPG");
                if (NewPicture.Exists())
                {
                    NewPicture.Delete();
                }
                FileOutputStream file = null;
                FileOutputStream newfile = null;

                if (data != null)
                {
                    try
                    {
                        file = new FileOutputStream(pictureFile);
                        file.Write(data);
                        file.Flush();

                        System.IO.MemoryStream imageStream = new System.IO.MemoryStream(data);
                        var rotatedBitmap = loadAndResizeBitmap(pictureFile.Path);
                        rotatedBitmap = Android.Graphics.Bitmap.CreateScaledBitmap(rotatedBitmap, 1080,1080, false);
                        System.IO.MemoryStream stream = new System.IO.MemoryStream();
                        rotatedBitmap.Compress(Android.Graphics.Bitmap.CompressFormat.Jpeg, 70, stream);
                        byte[] bitmapData = stream.ToArray();
                        newfile = new FileOutputStream(NewPicture);
                        newfile.Write(bitmapData);
                        newfile.Flush();
                        var CurrentStatus = System.Convert.ToString(NewPicture.Path);
                        MessagingCenter.Send(new MyMessage() { Myvalue = CurrentStatus }, "PictureTaken");
                    }
                    catch (FileNotFoundException e)
                    {


                    }
                    catch (IOException ie)
                    {

                    }
                    finally
                    {
                        file?.Close();
                    }
                }
                File deleteFile = new File(pictureFile.Path);
                bool deleted = deleteFile.Delete();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion Image Processing and send to shared class

        #region Image Resizing 
        Android.Graphics.Bitmap loadAndResizeBitmap(string filePath)
        {
            Android.Graphics.Bitmap resizedBitmap = Android.Graphics.BitmapFactory.DecodeFile(filePath);
            ExifInterface exif = null;
            try
            {
                exif = new ExifInterface(filePath);
                string orientation = exif.GetAttribute(ExifInterface.TagOrientation);

                Android.Graphics.Matrix matrix = new Android.Graphics.Matrix();
                if (Control.CameraID == 1)
                {
                    switch (orientation)
                    {
                        case "1":
                            matrix.PreRotate(-90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "3":
                            matrix.PreRotate(180);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "4":
                            matrix.PreRotate(180);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "5":
                            matrix.PreRotate(90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "6": // portrait
                            matrix.PreRotate(90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "7":
                            matrix.PreRotate(-90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "8":
                            matrix.PreRotate(-90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "0":
                            matrix.PreRotate(-90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                    }
                }
                else
                {
                    switch (orientation)
                    {
                        case "1":
                            matrix.PreRotate(90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "3":
                            matrix.PreRotate(180);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "4":
                            matrix.PreRotate(180);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "5":
                            matrix.PreRotate(90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "6": // portrait
                            matrix.PreRotate(90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "7":
                            matrix.PreRotate(90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "8":
                            matrix.PreRotate(90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                        case "0":
                            matrix.PreRotate(90);
                            resizedBitmap = Android.Graphics.Bitmap.CreateBitmap(resizedBitmap, 0, 0, resizedBitmap.Width, resizedBitmap.Height, matrix, false);
                            matrix.Dispose();
                            matrix = null;
                            break;
                    }
                }
                return resizedBitmap;
            }
            catch (IOException ex)
            {
                //Console.WriteLine("An exception was thrown when reading exif from media file...:" + ex.Message);
                return null;
            }

        }
        #endregion Image Resizing 

        public void OnShutter() { }

    }
}