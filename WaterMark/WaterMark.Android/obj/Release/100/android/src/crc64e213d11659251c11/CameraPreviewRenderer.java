package crc64e213d11659251c11;


public class CameraPreviewRenderer
	extends crc643f46942d9dd1fff9.ViewRenderer_2
	implements
		mono.android.IGCUserPeer,
		android.hardware.Camera.FaceDetectionListener,
		android.hardware.Camera.PictureCallback,
		android.hardware.Camera.ShutterCallback
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onFaceDetection:([Landroid/hardware/Camera$Face;Landroid/hardware/Camera;)V:GetOnFaceDetection_arrayLandroid_hardware_Camera_Face_Landroid_hardware_Camera_Handler:Android.Hardware.Camera/IFaceDetectionListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"n_onPictureTaken:([BLandroid/hardware/Camera;)V:GetOnPictureTaken_arrayBLandroid_hardware_Camera_Handler:Android.Hardware.Camera/IPictureCallbackInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"n_onShutter:()V:GetOnShutterHandler:Android.Hardware.Camera/IShutterCallbackInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("WaterMark.Droid.Custom_Camera.CameraPreviewRenderer, WaterMark.Android", CameraPreviewRenderer.class, __md_methods);
	}


	public CameraPreviewRenderer (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == CameraPreviewRenderer.class)
			mono.android.TypeManager.Activate ("WaterMark.Droid.Custom_Camera.CameraPreviewRenderer, WaterMark.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public CameraPreviewRenderer (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == CameraPreviewRenderer.class)
			mono.android.TypeManager.Activate ("WaterMark.Droid.Custom_Camera.CameraPreviewRenderer, WaterMark.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android", this, new java.lang.Object[] { p0, p1 });
	}


	public CameraPreviewRenderer (android.content.Context p0)
	{
		super (p0);
		if (getClass () == CameraPreviewRenderer.class)
			mono.android.TypeManager.Activate ("WaterMark.Droid.Custom_Camera.CameraPreviewRenderer, WaterMark.Android", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}


	public void onFaceDetection (android.hardware.Camera.Face[] p0, android.hardware.Camera p1)
	{
		n_onFaceDetection (p0, p1);
	}

	private native void n_onFaceDetection (android.hardware.Camera.Face[] p0, android.hardware.Camera p1);


	public void onPictureTaken (byte[] p0, android.hardware.Camera p1)
	{
		n_onPictureTaken (p0, p1);
	}

	private native void n_onPictureTaken (byte[] p0, android.hardware.Camera p1);


	public void onShutter ()
	{
		n_onShutter ();
	}

	private native void n_onShutter ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
